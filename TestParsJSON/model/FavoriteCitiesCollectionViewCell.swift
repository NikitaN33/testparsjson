//
//  FavoriteCitiesCollectionViewCell.swift
//  TestParsJSON
//
//  Created by Nikita Nechyporenko on 23.11.2018.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit

class FavoriteCitiesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
}
