//
//  WeatherSkreenVC.swift
//  TestParsJSON
//
//  Created by Nikita Nechyporenko on 07.11.2018.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit



class WeatherSkreenVC: UIViewController {
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var descriptionWeatherLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    @IBOutlet weak var firstDayDateLabel: UILabel!
    @IBOutlet weak var secondDayDateLabel: UILabel!
    @IBOutlet weak var thirdDayDateLabel: UILabel!
    @IBOutlet weak var fourthDayDateLabel: UILabel!
    
    @IBOutlet weak var firstDayTempLabel: UILabel!
    @IBOutlet weak var secondDayTempLabel: UILabel!
    @IBOutlet weak var thirdDayTempLabel: UILabel!
    @IBOutlet weak var fourthDayTempLabel: UILabel!
    
    @IBOutlet weak var iconWeatherImageView: UIImageView!
    
    @IBOutlet weak var firstDayIconImageView: UIImageView!
    @IBOutlet weak var secondDayIconImageView: UIImageView!
    @IBOutlet weak var thirdDayIconImageView: UIImageView!
    @IBOutlet weak var fourthDayIconImageView: UIImageView!
    
    @IBOutlet weak var firstDayButton: UIButton!
    @IBOutlet weak var secondDayButton: UIButton!
    @IBOutlet weak var thirdDayButton: UIButton!
    @IBOutlet weak var fourthDayButton: UIButton!
    
    var cityForWeather = ""
    var forecastOnFourDays = [Forecast]()
    var latitude: String?
    var longitude: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if latitude == nil {
            requestWeather(apiUrl: "http://api.openweathermap.org/data/2.5/weather?q=", city: cityForWeather)
            requestForecast(apiUrl: "http://api.openweathermap.org/data/2.5/forecast?q=", city: cityForWeather)
        } else {
            requestWeatherForLocation(apiUrl: "http://api.openweathermap.org/data/2.5/weather?", latitude: latitude!, longitude: longitude!)
            requestForecastForLocation(apiUrl: "http://api.openweathermap.org/data/2.5/forecast?", latitude: latitude!, longitude: longitude!)
        }
        print("CITY NAME = \(cityForWeather)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func requestForecast(apiUrl: String, city: String) {
        
        let apiString = ViewController().createUrlString(api: apiUrl, city: city)
        let urlString = apiString
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let weatherData = try JSONDecoder().decode(WeatherData.self, from: data)
                let cityName = weatherData.city!.name!
                var index = 0
                for _ in 0..<4 {
                    index += 8
                    let list = weatherData.list![index]
                    let main = list.main
                    let temp = String(Int(main!.temp!))
                    let tempMin = main!.temp_min!
                    let tempMax = main!.temp_max!
                    let humidity = String(Int(main!.humidity!))
                    let weather = list.weather!.first!
                    let descriptionMain = weather.main!
                    let icon = weather.icon!
                    let windSpeed = list.wind!.speed!
                    let dateStr = list.dt_txt!
                    let forecastOneDay = Forecast.init(name: cityName, temp: temp, tempMin: tempMin, tempMax: tempMax, humidity: humidity, icon: icon, windSpeed: windSpeed, date: dateStr, description: descriptionMain)
                    self.forecastOnFourDays.append(forecastOneDay)
                }
                kMainQueue.async {
                    self.firstDayTempLabel.text = self.forecastOnFourDays[0].temp! + " \u{00B0}" + "C"
                    self.firstDayIconImageView.image = UIImage(named: self.forecastOnFourDays[0].icon!)
                    self.firstDayDateLabel.text =  ViewController().formateDate(dateString: self.forecastOnFourDays[0].date!)
                    self.secondDayTempLabel.text = self.forecastOnFourDays[1].temp! + " \u{00B0}" + "C"
                    self.secondDayIconImageView.image = UIImage(named: self.forecastOnFourDays[1].icon!)
                    self.secondDayDateLabel.text = ViewController().formateDate(dateString: self.forecastOnFourDays[1].date!)
                    self.thirdDayTempLabel.text = self.forecastOnFourDays[2].temp! + " \u{00B0}" + "C"
                    self.thirdDayIconImageView.image = UIImage(named: self.self.forecastOnFourDays[2].icon!)
                    self.thirdDayDateLabel.text = ViewController().formateDate(dateString: self.forecastOnFourDays[2].date!)
                    self.fourthDayTempLabel.text = self.forecastOnFourDays[3].temp! + " \u{00B0}" + "C"
                    self.fourthDayIconImageView.image = UIImage(named: self.forecastOnFourDays[3].icon!)
                    self.fourthDayDateLabel.text = ViewController().formateDate(dateString: self.forecastOnFourDays[3].date!)
                }
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    func requestForecastForLocation(apiUrl: String, latitude: String, longitude: String) {
        let lat = "lat=" + latitude
        let long = "&lon=" + longitude
        let apiKey = "&APPID=7447a676e83a937bb338f7b427032adc&units=metric"
        let urlString = apiUrl + lat + long + apiKey
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let weatherData = try JSONDecoder().decode(WeatherData.self, from: data)
                let cityName = weatherData.city!.name!
                var index = 0
                for _ in 0..<4 {
                    index += 8
                    let list = weatherData.list![index]
                    let main = list.main
                    let temp = String(Int(main!.temp!))
                    let tempMin = main!.temp_min!
                    let tempMax = main!.temp_max!
                    let humidity = String(Int(main!.humidity!))
                    let weather = list.weather!.first!
                    let descriptionMain = weather.main!
                    let icon = weather.icon!
                    let windSpeed = list.wind!.speed!
                    let dateStr = list.dt_txt!
                    let forecastOneDay = Forecast.init(name: cityName, temp: temp, tempMin: tempMin, tempMax: tempMax, humidity: humidity, icon: icon, windSpeed: windSpeed, date: dateStr, description: descriptionMain)
                    self.forecastOnFourDays.append(forecastOneDay)
                }
                kMainQueue.async {
                    self.firstDayTempLabel.text = self.forecastOnFourDays[0].temp! + " \u{00B0}" + "C"
                    self.firstDayIconImageView.image = UIImage(named: self.forecastOnFourDays[0].icon!)
                    self.firstDayDateLabel.text =  ViewController().formateDate(dateString: self.forecastOnFourDays[0].date!)
                    self.secondDayTempLabel.text = self.forecastOnFourDays[1].temp! + " \u{00B0}" + "C"
                    self.secondDayIconImageView.image = UIImage(named: self.forecastOnFourDays[1].icon!)
                    self.secondDayDateLabel.text = ViewController().formateDate(dateString: self.forecastOnFourDays[1].date!)
                    self.thirdDayTempLabel.text = self.forecastOnFourDays[2].temp! + " \u{00B0}" + "C"
                    self.thirdDayIconImageView.image = UIImage(named: self.self.forecastOnFourDays[2].icon!)
                    self.thirdDayDateLabel.text = ViewController().formateDate(dateString: self.forecastOnFourDays[2].date!)
                    self.fourthDayTempLabel.text = self.forecastOnFourDays[3].temp! + " \u{00B0}" + "C"
                    self.fourthDayIconImageView.image = UIImage(named: self.forecastOnFourDays[3].icon!)
                    self.fourthDayDateLabel.text = ViewController().formateDate(dateString: self.forecastOnFourDays[3].date!)
                }
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    func requestWeather(apiUrl: String, city: String) {
        let apiString = ViewController().createUrlString(api: apiUrl, city: city)
        let urlString = apiString
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let weatherDataToday = try JSONDecoder().decode(WeatherTodayData.self, from: data)
                let cityName = weatherDataToday.name!
                let temp = String(Int(weatherDataToday.main!.temp!))
                let humidity = String(Int(weatherDataToday.main!.humidity!))
                let weather = weatherDataToday.weather.first!
                let descriptionMain = weather.main!
                let windSpeed = weatherDataToday.wind!.speed!
                let icon = weather.icon!
                let tempMin = weatherDataToday.main!.temp_min!
                let tempMax = weatherDataToday.main!.temp_max!
                weatherToday.removeAll()
                let weatherNow = Forecast.init(name: cityName, temp: temp, tempMin: tempMin, tempMax: tempMax, humidity: humidity, icon: icon, windSpeed: windSpeed, date: cityName, description: descriptionMain)
                weatherToday.append(weatherNow)
                kMainQueue.async {
                    self.iconWeatherImageView.image = UIImage(named: icon)
                    self.windLabel.text = String(windSpeed) + "m/s"
                    self.descriptionWeatherLabel.text = descriptionMain
                    self.humidityLabel.text = humidity + " %"
                    self.temperatureLabel.text = temp + " \u{00B0}" + "C"
                    self.cityNameLabel.text = cityName
                }
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    func requestWeatherForLocation(apiUrl: String, latitude: String, longitude: String) {
        let lat = "lat=" + latitude
        let long = "&lon=" + longitude
        let apiKey = "&APPID=7447a676e83a937bb338f7b427032adc&units=metric"
        let urlString = apiUrl + lat + long + apiKey
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let weatherDataToday = try JSONDecoder().decode(WeatherTodayData.self, from: data)
                let cityName = weatherDataToday.name!
                let temp = String(Int(weatherDataToday.main!.temp!))
                let humidity = String(Int(weatherDataToday.main!.humidity!))
                let weather = weatherDataToday.weather.first!
                let descriptionMain = weather.main!
                let windSpeed = weatherDataToday.wind!.speed!
                let icon = weather.icon!
                let tempMin = weatherDataToday.main!.temp_min!
                let tempMax = weatherDataToday.main!.temp_max!
                weatherToday.removeAll()
                let weatherNow = Forecast.init(name: cityName, temp: temp, tempMin: tempMin, tempMax: tempMax, humidity: humidity, icon: icon, windSpeed: windSpeed, date: cityName, description: descriptionMain)
                weatherToday.append(weatherNow)
                kMainQueue.async {
                    self.iconWeatherImageView.image = UIImage(named: icon)
                    self.windLabel.text = String(windSpeed) + "m/s"
                    self.descriptionWeatherLabel.text = descriptionMain
                    self.humidityLabel.text = humidity + " %"
                    self.temperatureLabel.text = temp + " \u{00B0}" + "C"
                    self.cityNameLabel.text = cityName
                }
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    func setupSkreenWeatherNow() {
        cityNameLabel.text = weatherToday[0].name!
        temperatureLabel.text = weatherToday[0].temp! + " \u{00B0}" + "C"
        iconWeatherImageView.image = UIImage(named: weatherToday[0].icon!)
        windLabel.text = String(weatherToday[0].windSpeed!) + "m/s"
        descriptionWeatherLabel.text = weatherToday[0].description!
        humidityLabel.text = weatherToday[0].humidity! + " %"
    }
    
    func setupGeneralViewWeather(indexForDay: Int) {
        cityNameLabel.text = forecastOnFourDays[indexForDay].name!
        temperatureLabel.text = forecastOnFourDays[indexForDay].temp! + " \u{00B0}" + "C"
        iconWeatherImageView.image = UIImage(named: forecastOnFourDays[indexForDay].icon!)
        windLabel.text = String(forecastOnFourDays[indexForDay].windSpeed!) + "m/s"
        descriptionWeatherLabel.text = forecastOnFourDays[indexForDay].description!
        humidityLabel.text = forecastOnFourDays[indexForDay].humidity! + " %"
    }
    
    @IBAction func addCtityToFavorite(_ sender: Any) {
        if favoriteCitisArrey.contains(cityForWeather) {
            print("УЖЕ ЕСТЬ ТАКОЙ ГОРОД!")
        } else {
            favoriteCitisArrey.append(cityForWeather)
        }
    }
    
    @IBAction func firstDayButton(_ sender: Any) {
        if firstDayButton.backgroundColor == UIColor.orange {
            firstDayButton.backgroundColor = UIColor.lightGray
            firstDayButton.alpha = 0.2
            setupSkreenWeatherNow()
        } else {
            firstDayButton.backgroundColor = UIColor.orange
            firstDayButton.alpha = 1
            secondDayButton.backgroundColor = UIColor.lightGray
            secondDayButton.alpha = 0.2
            thirdDayButton.backgroundColor = UIColor.lightGray
            thirdDayButton.alpha = 0.2
            fourthDayButton.backgroundColor = UIColor.lightGray
            fourthDayButton.alpha = 0.2
            setupGeneralViewWeather(indexForDay: 0)
            ViewController().formateDate(dateString: forecastOnFourDays[0].date!)
        }
    }
    
    @IBAction func secondDayButton(_ sender: Any) {
        if secondDayButton.backgroundColor == UIColor.orange {
            secondDayButton.backgroundColor = UIColor.lightGray
            secondDayButton.alpha = 0.2
            setupSkreenWeatherNow()
        } else {
            firstDayButton.backgroundColor = UIColor.lightGray
            firstDayButton.alpha = 0.2
            secondDayButton.backgroundColor = UIColor.orange
            secondDayButton.alpha = 1
            thirdDayButton.backgroundColor = UIColor.lightGray
            thirdDayButton.alpha = 0.2
            fourthDayButton.backgroundColor = UIColor.lightGray
            fourthDayButton.alpha = 0.2
            setupGeneralViewWeather(indexForDay: 1)
        }
    }
    
    @IBAction func thirdDayButton(_ sender: Any) {
        if thirdDayButton.backgroundColor == UIColor.orange {
            thirdDayButton.backgroundColor = UIColor.lightGray
            thirdDayButton.alpha = 0.2
            setupSkreenWeatherNow()
        } else {
            firstDayButton.backgroundColor = UIColor.lightGray
            firstDayButton.alpha = 0.2
            secondDayButton.backgroundColor = UIColor.lightGray
            secondDayButton.alpha = 0.2
            thirdDayButton.backgroundColor = UIColor.orange
            thirdDayButton.alpha = 1
            fourthDayButton.backgroundColor = UIColor.lightGray
            fourthDayButton.alpha = 0.2
            setupGeneralViewWeather(indexForDay: 2)
        }
    }
    
    @IBAction func fourthDayButton(_ sender: Any) {
        if fourthDayButton.backgroundColor == UIColor.orange {
            fourthDayButton.backgroundColor = UIColor.lightGray
            fourthDayButton.alpha = 0.2
            setupSkreenWeatherNow()
        } else {
            firstDayButton.backgroundColor = UIColor.lightGray
            firstDayButton.alpha = 0.2
            secondDayButton.backgroundColor = UIColor.lightGray
            secondDayButton.alpha = 0.2
            thirdDayButton.backgroundColor = UIColor.lightGray
            thirdDayButton.alpha = 0.2
            fourthDayButton.backgroundColor = UIColor.orange
            fourthDayButton.alpha = 1
            setupGeneralViewWeather(indexForDay: 3)
        }
    }
}
