//
//  FavoriteCitiesCollectionVC.swift
//  TestParsJSON
//
//  Created by Nikita Nechyporenko on 23.11.2018.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit
import CoreLocation

private let reuseIdentifier = "Cell"

var favoriteCitisArrey = ["Kiev", "Lviv", "Odessa", "London", "Boryspil"]

class FavoriteCitiesCollectionView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate {
    
    let locationManager: CLLocationManager = CLLocationManager()
    var weatherForFavoriteCities = [Forecast]()
    var choosedCity = ""
    var locationLat = ""
    var locationLong = ""

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        weatherForFavoriteCities = weatherForFavoriteCities.sorted { $0.name! < $1.name! }
        requestForAllFavoriteCities()
        collectionView.reloadData()
        
        locationManager.delegate = self
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMore" {
            let vc = segue.destination as? WeatherSkreenVC
            vc?.cityForWeather = choosedCity
        }
        if segue.identifier == "cityByLocation" {
            let vc = segue.destination as? WeatherSkreenVC
            vc?.latitude = locationLat
            vc?.longitude = locationLong
        }
    }
    
    func requestForAllFavoriteCities() {
        favoriteCitisArrey.forEach { (Str) in
            print(Str)
            requestWeather(apiUrl: "http://api.openweathermap.org/data/2.5/weather?q=", city: Str)
        }
    }
    
    func requestWeather(apiUrl: String, city: String) {
        let apiString = ViewController().createUrlString(api: apiUrl, city: city)
        let urlString = apiString
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let weatherDataToday = try JSONDecoder().decode(WeatherTodayData.self, from: data)
                let cityName = weatherDataToday.name!
                let temp = String(Int(weatherDataToday.main!.temp!))
                let humidity = String(Int(weatherDataToday.main!.humidity!))
                let weather = weatherDataToday.weather.first!
                let descriptionMain = weather.main!
                let windSpeed = weatherDataToday.wind!.speed!
                let icon = weather.icon!
                let tempMin = weatherDataToday.main!.temp_min!
                let tempMax = weatherDataToday.main!.temp_max!
                let weatherNow = Forecast.init(name: cityName, temp: temp, tempMin: tempMin, tempMax: tempMax, humidity: humidity, icon: icon, windSpeed: windSpeed, date: cityName, description: descriptionMain)
                self.weatherForFavoriteCities.append(weatherNow)
                kMainQueue.async {
                    self.collectionView.reloadData()
                }
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weatherForFavoriteCities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FavoriteCitiesCollectionViewCell
        
        cell.cityLabel.text = weatherForFavoriteCities[indexPath.row].name
        cell.backgroundColor = UIColor.darkGray
        cell.iconImageView.image = UIImage(named: weatherForFavoriteCities[indexPath.row].icon!)
        cell.tempLabel.text = weatherForFavoriteCities[indexPath.row].temp! + " \u{00B0}" + "C"
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        choosedCity = weatherForFavoriteCities[indexPath.row].name!
        return true
    }
    
    //MARK: CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(manager.location!)
        
        let currentLocation = locations.last as! CLLocation
        
        if (currentLocation.horizontalAccuracy > 0) {
//            locationManager.stopUpdatingLocation() //stop updating location
            
            let coord = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
            let latitude = String(coord.latitude)
            let longitude = String(coord.longitude)
            print("latitude  \(latitude)")
            print("longitude  \(longitude)")
            locationLat = latitude
            locationLong = longitude
//            requestWeatherForLocation(apiUrl: "http://api.openweathermap.org/data/2.5/weather?", latitude: latitude, longitude: longitude)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        print("location can not gotten")
    }
    
//    func requestWeatherForLocation(apiUrl: String, latitude: String, longitude: String) {
//        let lat = "lat=" + latitude
//        let long = "&lon=" + longitude
//        let apiKey = "&APPID=7447a676e83a937bb338f7b427032adc&units=metric"
//        let urlString = apiUrl + lat + long + apiKey
//
//        guard let url = URL(string: urlString) else { return }
//        URLSession.shared.dataTask(with: url) { (data, response, error) in
//            guard let data = data else { return }
//            guard error == nil else { return }
//            do {
//                let weatherDataToday = try JSONDecoder().decode(WeatherTodayData.self, from: data)
//                let cityName = weatherDataToday.name!
//                let temp = String(Int(weatherDataToday.main!.temp!))
//                let humidity = String(Int(weatherDataToday.main!.humidity!))
//                let weather = weatherDataToday.weather.first!
//                let descriptionMain = weather.main!
//                let windSpeed = weatherDataToday.wind!.speed!
//                let icon = weather.icon!
//                let tempMin = weatherDataToday.main!.temp_min!
//                let tempMax = weatherDataToday.main!.temp_max!
//                let weatherNow = Forecast.init(name: cityName, temp: temp, tempMin: tempMin, tempMax: tempMax, humidity: humidity, icon: icon, windSpeed: windSpeed, date: cityName, description: descriptionMain)
//
//                self.location = weatherNow.name!
//                print("CITY NAME = = = \(cityName)")
//            } catch let error {
//                print(error)
//            }
//            }.resume()
//    }

    @IBAction func locationButton(_ sender: Any) {
        
    }
    
}
