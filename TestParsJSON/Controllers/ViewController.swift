//
//  ViewController.swift
//  TestParsJSON
//
//  Created by Nikita Nechyporenko on 06.11.2018.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit

let kMainQueue = DispatchQueue.main
let kBgQ = DispatchQueue.global(qos: .background)

var weatherToday = [Forecast]()

class ViewController: UIViewController {
    
    let apiUrlForecast = "http://api.openweathermap.org/data/2.5/forecast?q="
   
    
//    func requestWeather(apiUrl: String, city: String) {
//        let apiString = self.createUrlString(api: apiUrl, city: city)
//        let urlString = apiString
//        guard let url = URL(string: urlString) else { return }
//        
//        URLSession.shared.dataTask(with: url) { (data, response, error) in
//            
//            guard let data = data else { return }
//            guard error == nil else { return }
//            do {
//                let weatherDataToday = try JSONDecoder().decode(WeatherTodayData.self, from: data)
//                let cityName = weatherDataToday.name!
//                let temp = String(Int(weatherDataToday.main!.temp!))
//                let tempMin = weatherDataToday.main!.temp_min!
//                let tempMax = weatherDataToday.main!.temp_max!
//                let humidity = String(Int(weatherDataToday.main!.humidity!))
//                let weather = weatherDataToday.weather.first!
//                let descriptionMain = weather.main!
//                let icon = weather.icon!
//                let windSpeed = weatherDataToday.wind!.speed!
//                let weatherNow = Forecast.init(name: cityName, temp: temp, tempMin: tempMin, tempMax: tempMax, humidity: humidity, icon: icon, windSpeed: windSpeed, date: cityName, description: descriptionMain)
//                weatherToday.append(weatherNow)
//            } catch let error {
//                print(error)
//            }
//            }.resume()
//    }
    
    func formateDate(dateString: String) -> String {
        
        let arrChar = [Character](dateString)
        let dayOfMounth = String(arrChar[8]) + String(arrChar[9])
        let mounthOfYear = String(arrChar[5]) + String(arrChar[6])
        let dayAndMounthn = dayOfMounth + "." + mounthOfYear
        let year = String(arrChar[0]) + String(arrChar[1]) + String(arrChar[2]) + String(arrChar[3])
        let formatedDate = dayAndMounthn + "." + year
        return formatedDate
    }
    
    func createUrlString(api: String, city: String) -> String {
        let apiKey = "&APPID=7447a676e83a937bb338f7b427032adc&units=metric"
        let urlStr = api + city + apiKey
        return urlStr
    }
    
}
