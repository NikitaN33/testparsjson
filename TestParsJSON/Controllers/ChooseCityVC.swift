//
//  ChooseCityVC.swift
//  TestParsJSON
//
//  Created by Nikita Nechyporenko on 07.11.2018.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit

class ChooseCityVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var citiesArr = ["Kiev", "Lviv", "Kharkiv", "Dnipropetrovsk", "Odessa", "Khmelnytskyy", "Mykolayiv", "Melitopol", "Donetsk", "Uman", "Ivano-Frankivsk", "Chernihiv", "Zhytomyr", "Ternopil", "Poltava", "Sumy", "Vinnytsya", "Mukacheve", "Mariupol", "London", "Paris", "Rome", "Moscow", "Berlin", "Boryspil"]
    
    var choosedCity = ""
    var searching = false
    var searchCity = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        citiesArr = citiesArr.sorted { $0 < $1 }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWeather" {
            let vc = segue.destination as? WeatherSkreenVC
            vc?.cityForWeather = choosedCity
        }
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return searchCity.count
        } else {
            return  citiesArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellCities = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if searching {
            cellCities.textLabel?.text = searchCity[indexPath.row]
        } else {
            cellCities.textLabel?.text = citiesArr[indexPath.row]
        }
        cellCities.detailTextLabel?.text = ""
        return cellCities
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searching {
            choosedCity = searchCity[indexPath.row]
        } else {
            choosedCity = citiesArr[indexPath.row]
        }
        performSegue(withIdentifier: "showWeather", sender: nil)
    }
    
    //MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var searchArr = [""]
        searchArr = citiesArr.filter { $0.contains(searchText) }
        searchCity = searchArr
        searching = true
        tableView.reloadData()
        print(searchArr)
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        tableView.reloadData()
    }
}

